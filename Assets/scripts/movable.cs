using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class movable : MonoBehaviour
{ 
    
    private TargetJoint2D _sj;
    [SerializeField]
    private int frequency = 1;
    [SerializeField]
    private float damping = 0.5f;

    void Awake()      
    {
        _sj = gameObject.AddComponent<TargetJoint2D>();
        _sj.target = gameObject.transform.position;
        _sj.enabled = false;
        _sj.dampingRatio = damping;
        _sj.frequency = frequency;
    }

    void Update()
    {
        if(gameObject.transform.position.y < -10)
        {
            Destroy(gameObject);
        }
    }

    void OnMouseDown() 
    {
        Debug.Log("Mouse down");
        _sj.enabled = true;
        _sj.target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void OnMouseDrag()  
    {
            _sj.target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private void OnMouseUp()
    {
        Debug.Log("Mouse up");
        _sj.enabled = false;
        _sj.target = gameObject.transform.position;
    }
}
