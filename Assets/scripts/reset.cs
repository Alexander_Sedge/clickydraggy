using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reset : MonoBehaviour
{
    [SerializeField]
    private goal resetTarget;
    [SerializeField]
    private gamemanager gm;

    private void OnMouseDown()
    {
        resetTarget.ResetPosition();
        //reset score
    }
}
