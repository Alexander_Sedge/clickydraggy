using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class more : MonoBehaviour
{
    [SerializeField]                                               //[SerializeField] means the value shows up in the editor, so you can adjust it without opening code.
    private Vector3 spawnPoint;                                   //The spot we want to create a new object.
    [SerializeField]
    private GameObject oreo;                                      //The object we want to create. (Serializing it means we can just drag a prefab onto the field in the editor!)

    private void OnMouseUp()
    {
        Instantiate(oreo, spawnPoint, Quaternion.identity);      //Spawn THIS object at THIS space with THIS rotation. Quaternion.identity just means "the same rotation it normally has."
    }

}
