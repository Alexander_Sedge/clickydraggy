using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goal : MonoBehaviour
{

    private Vector3 startPosition;
    private Quaternion startRotation;
    [SerializeField]
    private gamemanager gm;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = gameObject.transform.position;
        startRotation = gameObject.transform.rotation;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("oreo"))
        {
            Destroy(collision.gameObject);
            //increment score
        }
    }

    public void ResetPosition()
    {
        transform.position = startPosition;
        transform.rotation = startRotation;
    }
}
